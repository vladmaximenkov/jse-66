package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    private final TaskRepository repository;

    @Autowired
    public TaskService(@NotNull final TaskRepository repository) {
        this.repository = repository;
    }

    @NotNull
    public Collection<Task> findAll() {
        return repository.findAll();
    }

    public void merge(@NotNull final List<Task> list) {
        list.forEach(repository::add);
    }

    public void merge(@NotNull final Task task) {
        repository.add(task);
    }

    public void removeAll() {
        repository.clear();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return repository.findById(id);
    }

    public void removeById(@NotNull final String id) {
        repository.removeById(id);
    }

}

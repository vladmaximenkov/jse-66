package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.ITaskCollectionResource;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.service.TaskService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(value = "/api/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskCollectionEndpoint implements ITaskCollectionResource {

    @NotNull
    private final TaskService service;

    @Autowired
    public TaskCollectionEndpoint(@NotNull final TaskService service) {
        this.service = service;
    }

    @NotNull
    @Override
    @GetMapping
    public Collection<Task> get() { return service.findAll(); }

    @Override
    @PostMapping
    public void post(@NotNull @RequestBody final List<Task> tasks) {
        service.merge(tasks);
    }

    @Override
    @PutMapping
    public void put(@NotNull @RequestBody final List<Task> tasks) {
        service.merge(tasks);
    }

    @Override
    @DeleteMapping
    public void delete() {
        service.removeAll();
    }

}

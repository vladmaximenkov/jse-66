package ru.vmaksimenkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.vmaksimenkov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Project {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @NotNull
    private Status status = Status.NOT_STARTED;

    public Project(@Nullable final String name) {
        this.name = name;
    }

}

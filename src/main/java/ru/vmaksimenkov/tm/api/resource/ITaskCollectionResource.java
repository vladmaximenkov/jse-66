package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskCollectionResource {

    @NotNull
    @GetMapping
    Collection<Task> get();

    @PostMapping
    void post(@NotNull @RequestBody List<Task> tasks);

    @PutMapping
    void put(@NotNull @RequestBody List<Task> tasks);

    @DeleteMapping
    void delete();

}

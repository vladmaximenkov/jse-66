package ru.vmaksimenkov.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class Const {

    @NotNull public static final String HOST = "http://localhost:8080";

    @NotNull public static final String PROJECT = HOST + "/api/project";

    @NotNull public static final String PROJECTS = HOST + "/api/projects";

    @NotNull public static final String TASK = HOST + "/api/task";

    @NotNull public static final String TASKS = HOST + "/api/tasks";

}
